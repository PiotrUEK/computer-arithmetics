#include "dynarray.h"
#include <stdlib.h>

bool dynarrNew(DYNARRAY* array) {
    size_t initialCapacity = 2;
    void** elements = malloc(initialCapacity * sizeof(void*));
    if (!elements) {
        return false;
    }
    array->elements = elements;
    array->capacity = initialCapacity;
    array->size = 0;
    return true;
}

bool dynarrAdd(DYNARRAY* array, void* pointer) {
    if (array->size == array->capacity) {
        size_t newCapacity = 2 * array->capacity;
        void** newElements = realloc(array->elements, newCapacity * sizeof(void*));
        if (!newElements) {
            return false;
        }
        array->elements = newElements;
        array->capacity = newCapacity;
    }
    array->elements[array->size] = pointer;
    array->size++;
    return true;
}

int dynarrEnumerate(const DYNARRAY* array, dynarrEnumFun callback, void* customParam) {
    int retVal = 0;
    int i;
    for (i = 0; i < array->size; i++) {
        if (callback(array->elements[i], i, &retVal, customParam) == stopEnumeration) {
            break;
        }
    }
    return retVal;
}

void dynarrDelete(DYNARRAY* array) {
    free(array->elements);
    array->elements = NULL;
    array->capacity = 0;
    array->size = 0;
}