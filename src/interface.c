#include <stdlib.h>
#include "interface.h"
#include <string.h>
#include "dynarray.h"

DYNARRAY windows;
DYNARRAY binaryWindows;
DYNARRAY commandWindows;
DYNARRAY outputWindows;

enum {
    FLASH_COLOR_OFFSET = COLOR_WHITE+1
};

bool initInterface() {
    if (!dynarrNew(&windows) ||
        !dynarrNew(&binaryWindows) ||
        !dynarrNew(&commandWindows) ||
        !dynarrNew(&outputWindows) ||
        !initscr() ||
        start_color() == ERR ||
        keypad(stdscr, TRUE) == ERR ||
        mouse_on(ALL_MOUSE_EVENTS) == ERR) {
        return false;
    }
    curs_set(0);
    mouseinterval(0);
    refresh();
    short color;
    for (color = COLOR_BLACK; color <= COLOR_WHITE; color++) {
        init_pair(color, color, COLOR_BLACK);
        init_pair(color+FLASH_COLOR_OFFSET, color, COLOR_WHITE);
    }
    return true;
}

bool registerBinaryWindow(BINARYWINDOW* window) {
    return dynarrAdd(&windows, window->win) && dynarrAdd(&binaryWindows, window);
}

bool registerCommandWindow(COMMANDWINDOW* window) {
    return dynarrAdd(&windows, window->win) && dynarrAdd(&commandWindows, window);
}

bool registerOutputWindow(OUTPUTWINDOW* window) {
    return dynarrAdd(&windows, window->win) && dynarrAdd(&outputWindows, window);
}

dynarrEnumIterationResult delwinCallback(void* window, size_t index, int* retVal, void* customParam) {
    delwin((WINDOW*)window);
    return continueEnumeration;
}

void closeInterface() {
    dynarrEnumerate(&windows, delwinCallback, NULL);
    endwin();
    dynarrDelete(&windows);
    dynarrDelete(&binaryWindows);
    dynarrDelete(&commandWindows);
    dynarrDelete(&outputWindows);
}

void titleBox(WINDOW* win, const char* title) {
    box(win, 0, 0);
    int len = strlen(title);
    int titleLeft = (win->_maxx - len) / 2;
    int titleRight = titleLeft + len;
    mvwaddstr(win, 0, titleLeft, title);
    mvwaddch(win, 0, titleLeft-1, ' ');
    mvwaddch(win, 0, titleRight, ' ');
}

bool newBinaryWin(BINARYWINDOW* winStruct, int y, int x,
                  char* title, short color, int bitsize,
                  bitword* bits, bitword* selectedBits,
                  binaryWindowClickHandler onClick) {
    WINDOW* win = newwin(BIT_HEIGHT+3, bitsize*(BIT_WIDTH+1)+1, y, x);
    if (!win) {
        return false;
    }
    winStruct->win = win;
    winStruct->title = title;
    winStruct->color = color;
    winStruct->bitsize = bitsize;
    winStruct->bits = bits;
    winStruct->selectedBits = selectedBits;
    winStruct->onClick = onClick;
    if (!registerBinaryWindow(winStruct)) {
        delwin(win);
        return false;
    }
    return true;
}

bool newCommandWin(COMMANDWINDOW* winStruct, int y, int x, int width,
                  char* title, short color,
                  commandWindowClickHandler onClick) {
    WINDOW* win = newwin(COMMAND_HEIGHT, width, y, x);
    if (!win) {
        return false;
    }
    winStruct->win = win;
    winStruct->title = title;
    winStruct->color = color;
    winStruct->onClick = onClick;
    if (!registerCommandWindow(winStruct)) {
        delwin(win);
        return false;
    }
    wbkgd(win, ' ' | COLOR_PAIR(color) | A_BOLD);
    return true;
}

bool newOutputWin(OUTPUTWINDOW* winStruct, int y, int x, int width,
                  char* title, short color,
                  outputWindowGetTextHandler onGetText) {
    WINDOW* win = newwin(OUTPUT_HEIGHT, width, y, x);
    if (!win) {
        return false;
    }
    winStruct->win = win;
    winStruct->title = title;
    winStruct->color = color;
    winStruct->onGetText = onGetText;
    if (!registerOutputWindow(winStruct)) {
        delwin(win);
        return false;
    }
    wbkgd(win, ' ' | COLOR_PAIR(color) | A_BOLD);
    return true;
}

const chtype BIT_GLYPHS[2][BIT_HEIGHT][BIT_WIDTH] = {
    {
        { ACS_BLOCK, ACS_BLOCK, ACS_BLOCK },
        { ACS_BLOCK, ' ', ACS_BLOCK },
        { ACS_BLOCK, ACS_BLOCK, ACS_BLOCK }
    }, {
        { ' ', ACS_BLOCK, ' ' },
        { ' ', ACS_BLOCK, ' ' },
        { ' ', ACS_BLOCK, ' ' }
    }
};

void paintBit(WINDOW* win, int row, int col, unsigned char bit) {
    bit %= 2;
    int r, c;
    for (r = 0; r < BIT_HEIGHT; r++) {
        for (c = 0; c < BIT_WIDTH; c++) {
            mvwaddch(win, row+r, col+c, BIT_GLYPHS[bit][r][c]);
        }
    }
}

int bitToColumn(WINDOW* win, int bit) {
    return win->_maxx - (bit+1)*(BIT_WIDTH+1);
}

int columnToBit(WINDOW* win, int col) {
    return (win->_maxx-col-1)/(BIT_WIDTH+1);
}

void paintBitword(WINDOW* win, int row, bitword bits, int bitsize) {
    int b;
    for (b = 0; b < bitsize; b++) {
        paintBit(win, row, bitToColumn(win, b), bits & (1<<b) ? 1 : 0);
    }
    wrefresh(win);
}

void paintBitSelections(WINDOW* win, int row, bitword selectedBits, int bitsize) {
    int b;
    for (b = 0; b < bitsize; b++) {
        mvwaddch(win, row, bitToColumn(win, b) + BIT_WIDTH/2,
                 (selectedBits & (1<<b) ? 'v' : ' ') | COLOR_PAIR(COLOR_WHITE) | A_BOLD);
    }
    wrefresh(win);
}

void paintBinaryWin(const BINARYWINDOW* window) {
    wattrset(window->win, A_BOLD | COLOR_PAIR(window->color));
    paintBitword(window->win, 2, *window->bits, window->bitsize);
    paintBitSelections(window->win, 1, *window->selectedBits, window->bitsize);
    titleBox(window->win, window->title);
    wrefresh(window->win);
}

void paintCommandWin(const COMMANDWINDOW* window) {
    wattrset(window->win, A_BOLD | COLOR_PAIR(window->color));
    box(window->win, 0, 0);
    int len = strlen(window->title);
    int titleLeft = (window->win->_maxx - len) / 2;
    mvwaddstr(window->win, 1, titleLeft, window->title);
    wrefresh(window->win);
}

void paintOutputWin(const OUTPUTWINDOW* window) {
    wattrset(window->win, A_BOLD | COLOR_PAIR(window->color));
    if (window->onGetText) {
        mvwaddstr(window->win, 2, 2, window->onGetText());
    }
    titleBox(window->win, window->title);
    wrefresh(window->win);
}

dynarrEnumIterationResult repaintBinWndCallback(void* window, size_t index, int* retVal, void* customParam) {
    paintBinaryWin((BINARYWINDOW*)window);
    return continueEnumeration;
}

dynarrEnumIterationResult repaintCmdWndCallback(void* window, size_t index, int* retVal, void* customParam) {
    paintCommandWin((COMMANDWINDOW*)window);
    return continueEnumeration;
}

dynarrEnumIterationResult repaintOutWndCallback(void* window, size_t index, int* retVal, void* customParam) {
    paintOutputWin((OUTPUTWINDOW*)window);
    return continueEnumeration;
}

void repaintAll() {
    refresh();
    dynarrEnumerate(&binaryWindows, repaintBinWndCallback, NULL);
    dynarrEnumerate(&commandWindows, repaintCmdWndCallback, NULL);
    dynarrEnumerate(&outputWindows, repaintOutWndCallback, NULL);
}

dynarrEnumIterationResult findMouseBinWndCallback(void* binWindow, size_t index, int* retVal, void* foundBinWndPtr) {
    enum { found = 1 };
    int x, y;
    wmouse_position(((BINARYWINDOW*)binWindow)->win, &y, &x);
    if (x < 0) {
        return continueEnumeration;
    }
    *retVal = found;
    *(BINARYWINDOW**)foundBinWndPtr = (BINARYWINDOW*)binWindow;
    return stopEnumeration;
}

dynarrEnumIterationResult findMouseCmdWndCallback(void* cmdWindow, size_t index, int* retVal, void* foundCmdWndPtr) {
    enum { found = 1 };
    int x, y;
    wmouse_position(((COMMANDWINDOW*)cmdWindow)->win, &y, &x);
    if (x < 0) {
        return continueEnumeration;
    }
    *retVal = found;
    *(COMMANDWINDOW**)foundCmdWndPtr = (COMMANDWINDOW*)cmdWindow;
    return stopEnumeration;
}

bool dispatchBinaryWindowsMouseClick() {
    BINARYWINDOW* foundWindow;
    if (dynarrEnumerate(&binaryWindows, findMouseBinWndCallback, &foundWindow)) {
        int x, y;
        wmouse_position(foundWindow->win, &y, &x);
        if (foundWindow->onClick) {
            foundWindow->onClick(columnToBit(foundWindow->win, x));
        }
        return true;
    }
    return false;
}

bool dispatchCommandWindowsMouseClick() {
    COMMANDWINDOW* foundWindow;
    if (dynarrEnumerate(&commandWindows, findMouseCmdWndCallback, &foundWindow)) {
        wbkgd(foundWindow->win, ' ' | COLOR_PAIR(foundWindow->color+FLASH_COLOR_OFFSET));
        wrefresh(foundWindow->win);
        napms(100);
        wbkgd(foundWindow->win, ' ' | COLOR_PAIR(foundWindow->color));
        if (foundWindow->onClick) {
            foundWindow->onClick();
        }
        return true;
    }
    return false;
}

void eventLoop() {
    repaintAll();
    bool running = true;
    while (running) {
        int key = getch();
        switch (key) {
            case KEY_MOUSE:
                request_mouse_pos();
                short action = BUTTON_STATUS(1) & BUTTON_ACTION_MASK;
                if (BUTTON_CHANGED(1) &&
                    (action == BUTTON_CLICKED || action == BUTTON_PRESSED)) {
                    if (dispatchBinaryWindowsMouseClick()) {
                        repaintAll();
                        break;
                    }
                    if (dispatchCommandWindowsMouseClick()) {
                        repaintAll();
                        break;
                    }
                }
                break;
            case 033:   // ESCAPE
                running = false;
                break;
        }
    }
}
